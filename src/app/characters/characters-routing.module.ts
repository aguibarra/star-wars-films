import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CharactersComponent } from './characters.component';

const routes: Routes = [
    {
        path: 'film/:film',
        component: CharactersComponent
    },
    {
        path: '',
        component: CharactersComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CharactersRoutingModule { }
