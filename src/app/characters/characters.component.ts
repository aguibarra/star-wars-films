import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StarWarsService } from 'app/shared/services/starwars.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { take, map } from 'rxjs/operators';
import { Character } from 'app/shared/models/models';

@Component({
    selector: 'app-characters',
    templateUrl: 'characters.component.html'
})

export class CharactersComponent implements OnInit {
    genders = [];
    eyeColors = [];
    films = [];
    selectedGenders = [];
    selectedEyeColors = [];
    selectedFilms = [];
    dropdownSettings: IDropdownSettings = {};
    page = 1;
    pageSize = 10;
    characters: Character[] = [];
    charactersFiltered: Character[] = [];
    filmNames = [];
    allFilms = [];
    open: boolean;
    constructor(private activatedRoute: ActivatedRoute, private starSwarsService: StarWarsService) { }

    ngOnInit() {
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 5,
            allowSearchFilter: true
        };
        this.activatedRoute.params.subscribe(p => {
            this.setFilters(p['film']);
            this.onFilter()
        });
    }

    onFilter() {
        this.starSwarsService.getCharacters().pipe(
            map(c => c.filter(p => this.selectedEyeColors.some(e => e.item_id === p.eye_color))),
            map(c => c.filter(p => this.selectedGenders.some(e => e.item_id === p.gender))),
            map(c => c.filter(p => this.selectedFilms.some(e => p.films.indexOf(e.item_id) > -1))),
            take(1)
        ).subscribe(c => {
            this.characters = c;
            this.characters.forEach(c => { c.filmsObj = c.films.map(c => { return { url: c } }) })
            this.onPageChange();
        });
    }

    setFilters(film?: number) {
        this.starSwarsService.getGenders().pipe(take(1)).subscribe(g => {
            this.genders = g.map(e => { return { item_id: e, item_text: e } });
            this.selectedGenders = this.genders;
        });

        this.starSwarsService.getEyeColors().pipe(take(1)).subscribe(r => {
            this.eyeColors = r.map(e => { return { item_id: e, item_text: e } });
            this.selectedEyeColors = this.eyeColors;
        });

        this.starSwarsService.getFilms$.pipe(take(1)).subscribe(f => {
            this.films = f.map(t => { return { item_id: t.url, item_text: t.title } });
            f.map(t => this.filmNames[t.url] = t.title);
            f.map(t => this.allFilms[t.url] = t);

            this.selectedFilms = this.films;
            if (film) {
                this.selectedFilms = [this.films.find(f => f.item_id === `https://swapi.co/api/films/${film}/`)];
            }
        });
    }

    onPageChange() {
        const begin = (this.page - 1) * this.pageSize;
        this.charactersFiltered = this.characters.slice(begin, begin + this.pageSize);
    }

    getFilmObjects(filmUrls: string[]) {
        return filmUrls.map(c => { return { url: c } });
    }
}