import { NgModule } from '@angular/core';

import { CharactersComponent } from './characters.component';
import { CharactersRoutingModule } from './characters-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { NgbPagination, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [CommonModule, CharactersRoutingModule, NgMultiSelectDropDownModule.forRoot(),FormsModule, NgbPaginationModule],
    exports: [],
    declarations: [CharactersComponent],
    providers: [],
})
export class CharactersModule { }
