import { Routes } from '@angular/router';

export const Full_ROUTES: Routes = [
  {
    path: 'films',
    loadChildren: () => import('../../films/films.module').then(m => m.FilmsModule)
  },
  {
    path: 'characters',
    loadChildren: () => import('../../characters/characters.module').then(m => m.CharactersModule)
  },
  // {
  //   path: '',
  //   redirectTo: 'films',
  //   pathMatch: 'full'
  // }
];
