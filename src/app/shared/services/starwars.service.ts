import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { PaginatedModel, Film, Character } from '../models/models';
import { filter, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class StarWarsService {
    characters: Character[] = [];
    films: Film[] = [];
    getCharacters$: ReplaySubject<Character[]> = new ReplaySubject();
    getFilms$: ReplaySubject<Film[]> = new ReplaySubject();
    constructor(private httpClient: HttpClient) {
        this.getAllCharacters();
        this.getFilms();
    }

    private getFilms() {
        this.httpClient.get<PaginatedModel<Film>>(environment.apiUrlBase + 'films/').subscribe(f => {
            this.films = f.results;
            this.getFilms$.next(this.films);
        });
    }

    private getAllCharacters(url?: string) {
        this.httpClient.get<PaginatedModel<Character>>(url ? url : environment.apiUrlBase + 'people/').subscribe(d => {
            d.results.map(d => this.characters.push(d));
            if (d.next) {
                this.getAllCharacters(d.next);
            } else {
                this.getCharacters$.next(this.characters);
            }
        });
    }

    getCharacter(url: string): Observable<Character> {
        return this.httpClient.get<Character>(url);
    }

    getEyeColors() {
        return this.getCharacters().pipe(map(c => Array.from(new Set(c.map(d => d.eye_color)))));
    }

    getGenders() {
        return this.getCharacters().pipe(map(c => Array.from(new Set(c.map(d => d.gender)))));
    }

    getCharacters(film?: number) {
        if (film) {
            return this.getCharacters$.pipe(map(c => c.filter(c => c.films.indexOf(`https://swapi.co/api/films/${film}/`) > -1)));
        }
        return this.getCharacters$;
    }

}