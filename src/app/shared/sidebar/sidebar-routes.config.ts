import { RouteInfo } from './sidebar.metadata';
import { Injectable } from '@angular/core';

@Injectable()
export class RoutesService {
  constructor() {}
  get ROUTES(): RouteInfo[] {
    return [
      {
        path: '/films',
        title: 'Films',
        icon: 'ft-file-text',
        class: '',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: []
      },
      {
        path: '/characters',
        title: 'Characters',
        icon: 'ft-file-text',
        class: '',
        badge: '',
        badgeClass: '',
        isExternalLink: false,
        submenu: []
      }
    ];
  }
}
