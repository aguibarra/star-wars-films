import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmsRoutingModule } from './films-routing.module';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { FilmsComponent } from './films.component';

@NgModule({
  imports: [
    CommonModule,
    FilmsRoutingModule,
    NgbProgressbarModule
  ],
  declarations: [FilmsComponent]
})
export class FilmsModule {}
