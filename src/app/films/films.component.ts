import {
  Component,
  OnInit
} from '@angular/core';
import { StarWarsService } from 'app/shared/services/starwars.service';
import { Film, Character } from 'app/shared/models/models';
import { take, map } from 'rxjs/operators';
import { pipe, combineLatest } from 'rxjs';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html'
})
export class FilmsComponent implements OnInit {
  films: Film[] = [];
  hoveredFilm: Film;
  constructor(
    public starWarsService: StarWarsService
  ) { }

  ngOnInit(): void {
    this.starWarsService.getFilms$.pipe(take(1)).subscribe(f => this.films = f);
  }

  mouseOver(){
    console.log('hola')
  }

}
