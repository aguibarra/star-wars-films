import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {
  hideSidebar: boolean;

  options = {
    direction: 'ltr'
  };
  constructor() {}

  ngOnInit() {}


  isMobile() {
    return (
      navigator.maxTouchPoints || 'ontouchstart' in document.documentElement
    );
  }

  toggleHideSidebar($event: boolean): void {
    setTimeout(() => {
      this.hideSidebar = $event;
    }, 0);
  }

}
